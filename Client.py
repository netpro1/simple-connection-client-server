import socket


s = socket.socket()


port = 12345


s.connect(("127.0.0.1", port))

number = 1

while number < 100:
    data = s.recv(1024).decode()
    if not data:
        break

    number = int(data)
    print(number)

    number += 1
    if number > 100:
        break

    s.sendall(str(number).encode())

s.close()
