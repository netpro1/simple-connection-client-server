import socket


s = socket.socket()
print("Socket successfully created")


port = 12345

s.bind(("", port))
print("socket binded to %s" % (port))


s.listen(5)
print("socket is listening")

conn, addr = s.accept()
print("Connected by {}".format(addr))

number = 0

while number < 100:
    number += 1
    conn.sendall(str(number).encode())
    data = conn.recv(1024).decode()
    number = int(data)
    print(number)

conn.close()
s.close()
